package com.horical.gitographview;

import java.util.List;

interface IGraphView {

    void setHasFixedSize(boolean fixedSize);

    void setLinesData(List<GraphMultiPoint> linesData);

    void setMaxHorizontal(int maxHorizontal);

    void setMinVertical(double minVertical);

    void setMaxVertical(double maxVertical);

    void setVerticalUnit(String unit);

    void setHorizontalUnit(String unit);

    void setGraphType(int type);

    void setHighlightValue(int currentValue);

    void setDeadline(int start, int end, boolean visible);

    void setColorHighlight(int color);

    void setVerticalTextColor(int color);

    void setHorizontalTextColor(int color);
}
