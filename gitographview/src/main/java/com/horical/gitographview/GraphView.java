package com.horical.gitographview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class GraphView
        extends ConstraintLayout
        implements DataView.DataViewListener, IGraphView {

    public static final int TYPE_LINE = 0;
    public static final int TYPE_COLUMN = 1;
    public static final int TYPE_COLUMN_IN_COLUMN = 2;

    protected boolean hasFixedSize = false;
    protected DataView dataView;
    protected HorizontalScrollView dataScroll;
    protected VerticalView verticalView;
    protected HorizontalView horizontalView;
    protected HorizontalScrollView graphScroll;

    // Line Data
    protected List<GraphMultiPoint> lines;
    protected boolean negative = false;
    protected int typeGraph = TYPE_LINE;

    public GraphView(Context context) {
        super(context);
        init(context, null);
    }

    public GraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @SuppressLint({"NewApi", "UseSparseArrays"})
    private void init(Context context, @Nullable AttributeSet attrs) {
        setLayerType(LAYER_TYPE_SOFTWARE, null);

        // Init List Line
        if (lines == null)
            lines = new ArrayList<>();

        // Init Vertical View
        verticalView = new VerticalView(context, attrs);
        verticalView.setId(View.generateViewId());
        verticalView.setLayoutParams(new ConstraintLayout.LayoutParams((int) parseDp(50), 0));

        // Init Data View
        dataView = new DataView(context, attrs);
        if (typeGraph == TYPE_COLUMN_IN_COLUMN) {
            dataView.setLines(new ArrayList<Object>(lines.get(0).getPoints()));
        } else {
            dataView.setLines(new ArrayList<Object>(lines));
        }
        dataView.setListener(this);
        dataScroll = new HorizontalScrollView(context, attrs);
        dataScroll.setLayoutParams(new ConstraintLayout.LayoutParams(0, (int) parseDp(30)));
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.addView(dataView);
        dataScroll.setId(View.generateViewId());
        dataScroll.setHorizontalScrollBarEnabled(false);
        dataScroll.setSmoothScrollingEnabled(true);
        dataScroll.addView(linearLayout);

        // Init Graph Scroll View
        horizontalView = new HorizontalView(context, attrs);
        horizontalView.setLines(lines);
        horizontalView.setNegative(negative);
        horizontalView.setGraphType(typeGraph);
        horizontalView.setHasFixedSize(hasFixedSize);
        graphScroll = new HorizontalScrollView(context, attrs);
        graphScroll.setLayoutParams(new ConstraintLayout.LayoutParams(0, 0));
        linearLayout = new LinearLayout(context);
        linearLayout.addView(horizontalView);
        graphScroll.addView(linearLayout);
        graphScroll.setId(View.generateViewId());
        graphScroll.setHorizontalScrollBarEnabled(false);
        graphScroll.setSmoothScrollingEnabled(true);

        // Add View Child
        this.addView(verticalView);
        this.addView(dataScroll);
        this.addView(graphScroll);

        // Set ConstrainPosition
        ConstraintSet set = new ConstraintSet();
        set.clone(this);
        setVerticalViewConstrainPosition(set);
        setDataViewConstrainPosition(set);
        setHorizontalViewConstrainPosition(set);
        set.applyTo(this);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        verticalView.setVerticalTop((int) parseDp(30));
        verticalView.setVerticalBottom(graphScroll.getHeight());
        verticalView.setNegative(negative);

        horizontalView.setGraphType(typeGraph);
        horizontalView.setHasFixedSize(hasFixedSize);
        horizontalView.setHorizontalLeft(verticalView.getWidth());
        horizontalView.setHorizontalTop(dataScroll.getHeight());
        horizontalView.setMaxValueOy(verticalView.getMaxValueOy());
        horizontalView.setMinValueOy(verticalView.getMinValueOy());

        dataView.setPaddingLeft(verticalView.getWidthOfUnitType());
        dataView.setDataLeft(verticalView.getLayoutParams().width);
    }

    /**
     * @param set
     */
    private void setDataViewConstrainPosition(ConstraintSet set) {
        set.connect(
                dataScroll.getId(),
                ConstraintSet.LEFT,
                verticalView.getId(),
                ConstraintSet.RIGHT
        );
        set.connect(
                dataScroll.getId(),
                ConstraintSet.RIGHT,
                ConstraintSet.PARENT_ID,
                ConstraintSet.RIGHT
        );
        set.connect(
                dataScroll.getId(),
                ConstraintSet.TOP,
                ConstraintSet.PARENT_ID,
                ConstraintSet.TOP
        );
        set.connect(
                dataScroll.getId(),
                ConstraintSet.BOTTOM,
                graphScroll.getId(),
                ConstraintSet.TOP
        );
    }

    /**
     * @param set
     */
    private void setHorizontalViewConstrainPosition(ConstraintSet set) {
        set.connect(
                graphScroll.getId(),
                ConstraintSet.LEFT,
                verticalView.getId(),
                ConstraintSet.RIGHT
        );
        set.connect(
                graphScroll.getId(),
                ConstraintSet.RIGHT,
                ConstraintSet.PARENT_ID,
                ConstraintSet.RIGHT
        );
        set.connect(
                graphScroll.getId(),
                ConstraintSet.TOP,
                dataScroll.getId(),
                ConstraintSet.BOTTOM
        );
        set.connect(
                graphScroll.getId(),
                ConstraintSet.BOTTOM,
                ConstraintSet.PARENT_ID,
                ConstraintSet.BOTTOM
        );
    }

    /**
     * @param set
     */
    private void setVerticalViewConstrainPosition(ConstraintSet set) {
        set.connect(
                verticalView.getId(),
                ConstraintSet.LEFT,
                ConstraintSet.PARENT_ID,
                ConstraintSet.LEFT
        );
        set.connect(
                verticalView.getId(),
                ConstraintSet.TOP,
                ConstraintSet.PARENT_ID,
                ConstraintSet.TOP
        );
        set.connect(
                verticalView.getId(),
                ConstraintSet.BOTTOM,
                ConstraintSet.PARENT_ID,
                ConstraintSet.BOTTOM
        );
    }

    private double roundUpMaxValue(double maxVertical) {
        double temp = maxVertical;
        int i = 0;
        while ((temp /= 10) >= 10) {
            i++;
        }

        if ((maxVertical % (int) (Math.pow(10, i + 1) + 0.5)) != 0) {
            temp = (int) temp;
            temp++;
        }

        for (; i >= 0; i--) {
            temp *= 10;
        }
        return temp;
    }

    private double roundUpMinValue(double minVertical) {
        return -roundUpMaxValue(Math.abs(minVertical));
    }

    /**
     * @param px
     * @return
     */
    protected float parseDp(int px) {
        return (px * getContext().getResources().getDisplayMetrics().density + 0.5f);
    }

    @Override
    public void onDataViewClicked() {
        horizontalView.invalidate();
    }

    @Override
    public void setHasFixedSize(boolean hasFixedSize) {
        this.hasFixedSize = hasFixedSize;
        this.horizontalView.setHasFixedSize(hasFixedSize);
    }

    @Override
    public void setLinesData(List<GraphMultiPoint> linesData) {
        this.lines = linesData;
        this.horizontalView.setLines(linesData);
        if (typeGraph == TYPE_COLUMN_IN_COLUMN) {
            this.dataView.setLines(new ArrayList<Object>(linesData.get(0).getPoints()));
        } else {
            this.dataView.setLines(new ArrayList<Object>(linesData));
        }
    }

    @Override
    public void setMaxHorizontal(int maxHorizontal) {
        this.horizontalView.setMaxOx(maxHorizontal);
    }

    @Override
    public void setMinVertical(double minVertical) {
        this.verticalView.setMinValueOy(roundUpMinValue(minVertical));
        this.negative = minVertical < 0;
        this.verticalView.setNegative(negative);
        this.horizontalView.setNegative(negative);
    }

    @Override
    public void setMaxVertical(double maxVertical) {
        this.verticalView.setMaxValueOy(roundUpMaxValue(maxVertical));
    }

    @Override
    public void setVerticalUnit(String unit) {
        this.verticalView.setUnitType(unit);
    }

    @Override

    public void setHorizontalUnit(String unit) {
        this.horizontalView.setUnitType(unit);
    }

    @Override
    public void setGraphType(int type) {
        this.typeGraph = type;
        this.horizontalView.setGraphType(type);
        this.dataView.setGraphType(type);
    }

    @Override
    public void setHighlightValue(int currentValue) {
        horizontalView.setHighlightValue(currentValue);
    }

    @Override
    public void setDeadline(int start, int end, boolean bool) {
        this.horizontalView.setDeadline(start, end, bool);
    }

    @Override
    public void setColorHighlight(int color) {
        horizontalView.setColorHighlight(color);
    }

    @Override
    public void setVerticalTextColor(int color) {
        verticalView.setTextColor(color);
    }

    @Override
    public void setHorizontalTextColor(int color) {
        horizontalView.setTextColor(color);
    }

    public void reload() {
        dataView.requestLayout();
        dataScroll.invalidate();
        dataScroll.scrollTo(0, 0);
        verticalView.calculatorMeasureWidth();
        verticalView.requestLayout();
        horizontalView.setHorizontalLeft(verticalView.getMeanSureWidth());
        horizontalView.setMaxValueOy(verticalView.getMaxValueOy());
        horizontalView.setMinValueOy(verticalView.getMinValueOy());
        horizontalView.requestLayout();
        graphScroll.invalidate();
        graphScroll.scrollTo((int) horizontalView.getXScroll(), 0);
    }
}
