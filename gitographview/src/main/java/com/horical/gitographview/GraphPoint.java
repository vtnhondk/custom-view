package com.horical.gitographview;

import java.io.Serializable;

public class GraphPoint extends BaseGraphDto implements Serializable {
    GraphPoint() {

    }

    GraphPoint(Builder builder) {
        this.name = builder.name;
        this.color = builder.color;
        this.valueOx = builder.valueOx;
        this.valueOy = builder.valueOy;
        this.visible = true;
    }

    GraphPoint(float valueOx, double valueOy) {
        this.valueOx = valueOx;
        this.valueOy = valueOy;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String name;
        private int color;
        private double valueOx;
        private double valueOy;

        private Builder() {

        }

        public GraphPoint build() {
            return new GraphPoint(this);
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setColor(int color) {
            this.color = color;
            return this;
        }

        public Builder setValueOx(double valueOx) {
            this.valueOx = valueOx;
            return this;
        }

        public Builder setValueOy(double valueOy) {
            this.valueOy = valueOy;
            return this;
        }
    }
}
