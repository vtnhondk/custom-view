package com.horical.gitographview;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

public abstract class BaseGraphPartView extends View {

    protected Paint paint;
    protected int textColor;
    protected float textSize;

    public BaseGraphPartView(Context context) {
        super(context);
        init(context);
    }

    public BaseGraphPartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseGraphPartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected void init(Context context) {
        paint = new Paint();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setTextSize(textSize);
    }

    protected int getColor(int resourceId) {
        return ContextCompat.getColor(getContext(), resourceId);
    }

    protected String getString(int resourceId) {
        return getContext().getString(resourceId);
    }

    protected float parseDp(int px) {
        return (px * getContext().getResources().getDisplayMetrics().density + 0.5f);
    }

    protected Rect getBoundsText(String text) {
        Rect bounds = new Rect();
        try {
            paint.getTextBounds(
                    text,
                    0,
                    text.length(),
                    bounds
            );
        } catch (NullPointerException ex) {
            throw new IllegalArgumentException("Name text can not null. Check your lines data");
        }
        return bounds;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }
}
