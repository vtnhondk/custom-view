package com.horical.horicalcustomview;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.horical.gitographview.GraphMultiPoint;
import com.horical.gitographview.GraphPoint;
import com.horical.gitographview.GraphView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //region === EXAMPLE LINE CHART VIEW ===
//        final LineChartView lineChart = (LineChartView) findViewById(R.id.linechart);
//        lineChart.setChartData(getWalkingData());
//
//        findViewById(R.id.walkning_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lineChart.setChartData(getWalkingData());
//            }
//        });
//        findViewById(R.id.running_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lineChart.setChartData(getRuningData());
//            }
//        });
//        findViewById(R.id.cycling_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                lineChart.setChartData(getCyclingData());
//            }
//        });
        //endregion === EXAMPLE LINE CHART VIEW ===

        GraphView graphView = findViewById(R.id.graph_view);
        List<GraphMultiPoint> lines = new ArrayList<>();
        lines.clear();
        final Random random = new Random();
        double maxSum = 0;

        //region === DATA TYPE COLUMN IN COLUMN ===
        int[] colors = {Color.RED, Color.GRAY, Color.DKGRAY, Color.CYAN, Color.GREEN, Color.YELLOW};
        for (int i = 0; i < 31; i++) {
            List<GraphPoint> points = new ArrayList<>();
            for (int j = 0; j < 6; j++) {
                points.add(
                        GraphPoint.builder()
                                .setColor(colors[j])
                                .setName("New " + j)
                                .setValueOx(i + 1)
                                .setValueOy(random.nextInt(10))
                                .build()
                );
            }
            double sum = 0;
            for (GraphPoint point : points) {
                sum += point.getValueOy();
            }
            if (maxSum < sum) maxSum = sum;
            lines.add(new GraphMultiPoint(i + 1, sum, points));
        }
        //endregion === DATA TYPE COLUMN IN COLUMN ===

        //region === DATA TYPE LINE ===
//        List<GraphPoint> points = new ArrayList<>();
//        for (int i = 1; i <= 31; i++) {
//            points.add(
//                    GraphPoint.builder()
//                            .setValueOx(i)
//                            .setValueOy(random.nextInt(100))
//                            .build()
//            );
//        }
//        lines.add(new GraphMultiPoint("Done Task", Color.BLUE, points));

//        points = new ArrayList<>();
//        for (int i = 1; i <= 31; i++) {
//            points.add(
//                    GraphPoint.builder()
//                            .setValueOx(i)
//                            .setValueOy(random.nextInt(100))
//                            .build()
//            );
//        }
//        lines.add(new GraphMultiPoint("Close Task", Color.GREEN, points));
//
//        points = new ArrayList<>();
//        for (int i = 1; i <= 31; i++) {
//            points.add(
//                    GraphPoint.builder()
//                            .setValueOx(i)
//                            .setValueOy(random.nextInt(100))
//                            .build()
//            );
//        }
//        lines.add(new GraphMultiPoint("Started Task", Color.YELLOW, points));
//
//        points = new ArrayList<>();
//        for (int i = 1; i <= 31; i++) {
//            points.add(
//                    GraphPoint.builder()
//                            .setValueOx(i)
//                            .setValueOy(random.nextInt(100))
//                            .build()
//            );
//        }
//        lines.add(new GraphMultiPoint("Ended Task", Color.MAGENTA, points));
        //endregion === DATA TYPE LINE ===

        //region === DATA TYPE COLUMN ===
//        List<GraphPoint> points = new ArrayList<>();
//        for (int i = 1; i <= 31; i++) {
//            points.add(
//                    GraphPoint.builder()
//                            .setValueOx(i)
//                            .setValueOy(random.nextInt(100))
//                            .build()
//            );
//        }
//        lines.add(new GraphMultiPoint("Ended Task", Color.MAGENTA, points));
        //endregion === DATA TYPE COLUMN ===

        graphView.setGraphType(GraphView.TYPE_COLUMN_IN_COLUMN);
        graphView.setLinesData(lines);
        graphView.setMaxVertical(maxSum);
        graphView.setMinVertical(0);
        graphView.setMaxHorizontal(31);
        graphView.setDeadline(2, 6, true);
//        graphView.setHasFixedSize(true);
        graphView.setHighlightValue(1);
    }

    private float[] getWalkingData() {
        return new float[]{10, 12, 7, 14, 15, 19, 13, 2, 10, 13, 13, 10, 15, 14};
    }

    private float[] getRuningData() {
        return new float[]{22, 14, 20, 25, 32, 27, 26, 21, 19, 26, 24, 30, 29, 19};
    }

    private float[] getCyclingData() {
        return new float[]{0, 0, 0, 10, 14, 23, 40, 35, 32, 37, 41, 32, 18, 39};
    }
}
